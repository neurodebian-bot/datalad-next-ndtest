"""Custom types and dataclasses

.. currentmodule:: datalad_next.types
.. autosummary::
   :toctree: generated

   annexkey
   archivist
   enums
"""
