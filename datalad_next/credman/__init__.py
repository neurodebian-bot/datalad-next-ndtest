"""Credential management

.. currentmodule:: datalad_next.credman
.. autosummary::
   :toctree: generated

   manager
"""
from .manager import CredentialManager
