"""Configuration query and manipulation

This modules imports the central ``ConfigManager`` class from DataLad core.
"""

from datalad.config import ConfigManager
